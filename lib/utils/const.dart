import 'package:flutter/material.dart';

const Icon navig_nxt = Icon(Icons.navigate_next, color: Color(0x56232C2D));
const Icon cancel = Icon(Icons.backspace_outlined, color: Color(0xFF000000));